const webpack = require('webpack');
const path = require('path');

module.exports = {
    entry: [
        'react-hot-loader/patch',
        path.join(__dirname, './app/index.js')
    ],
    output: {
        filename: 'bundle.js',
        path: __dirname + '/app',
        publicPath: '/'
    },
    module: {
        rules: [{
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: ['babel-loader']
        }]
    },
    resolve: {
        modules: [
            'node_modules',
            path.resolve(__dirname, 'app')
        ],
        extensions: ['*', '.js', '.jsx']
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin()
    ],
    devtool: 'cheap-eval-source-map',
    devServer: {
        host: 'localhost',
        port: 9090,
        publicPath: '/',
        contentBase: path.join(__dirname, 'app'),
        hot: true,
        headers: { 'Access-Control-Allow-Origin': '*' }
    }
};
