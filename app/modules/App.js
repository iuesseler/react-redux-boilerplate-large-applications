import React from 'react';
import { UsersPage } from '../modules/users/components/UsersPage';

class App extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <h1> React Redux Starter </h1>
                <div>
                    <UsersPage/>
                </div>
            </div>
        );
    }
}

export { App };
