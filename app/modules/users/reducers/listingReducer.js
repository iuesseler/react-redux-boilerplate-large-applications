import * as types from "../actionTypes";

const initialState = () => {
    return Object.assign({}, {
        users: []
    });
};

function handleLoadUsersSuccess(state, action) {
    const users = action.payload;
    return { ...state, users: users };
}

function reducer(state = initialState(), action) {
    switch (action.type) {
        case types.USERS_LOAD_USERS_SUCCESS: {
            return handleLoadUsersSuccess(state, action);
        }
        default: {
            return state;
        }
    }
}
export default reducer;
