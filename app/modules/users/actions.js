import * as api from "../../api";
import * as types from "./actionTypes";

export function loadUsers() {
    return dispatch => {
        api.getUsers().then(result => result.data).then(users => {
            dispatch({ type: types.USERS_LOAD_USERS_SUCCESS, payload: users.items});
        });
    };
}
