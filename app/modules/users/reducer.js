import { combineReducers } from "redux";
import listingReducer from "./reducers/listingReducer";

const reducer = combineReducers({
    listing: listingReducer
});

export { reducer };
