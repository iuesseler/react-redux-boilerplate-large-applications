import React from 'react';
import * as usersActions from "../actions";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

class UsersPage extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { actions } = this.props;
        actions.users.loadUsers();
    }

    createUsersList = (users) => {
        const list = users.map((user, i) => <li key={i}> { user.name } </li> );
        return list;
    };

    render() {
        const  { users } = this.props;
        const usersList = this.createUsersList(users);
        return (
            <div>
                <h4> Users </h4>
                <ul>
                   { usersList }
                </ul>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        users : state.users.listing.users
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: {
            users: bindActionCreators(usersActions, dispatch)
        }
    };
}

const usersPage = connect(mapStateToProps, mapDispatchToProps)(UsersPage);

export { usersPage as UsersPage };
