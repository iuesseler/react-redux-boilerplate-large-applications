import { routerReducer } from "react-router-redux";
import { combineReducers } from "redux";
import { reducer as usersReducer } from "./modules/users/reducer";

const rootReducer = combineReducers ({
    routing: routerReducer,
    users: usersReducer
});

export default (state, action) => {
    return rootReducer(state, action);
};
