import axios from "axios";

const mainInstance = axios.create();

export function getUsers() {
    return mainInstance.get(`http://assets.studio71.io/test/news_feed.json`);
}
