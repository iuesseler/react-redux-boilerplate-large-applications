## React/Redux Boilerplate for large scale applications

npm install
npm start
node version 9.9.0

1. Redux template created for single page: includes actionTypes, actions, reducer, and connection to React Smart component.
2. Redux Thunk middleware added for handling Async API calls
3. Redux Web Dev. Tools for chrome extension for debugging
4. Eslint file for JSX and ES6 linting
5. Webpack configed ES6/Babel/JSX and webpack Dev Server.


---
